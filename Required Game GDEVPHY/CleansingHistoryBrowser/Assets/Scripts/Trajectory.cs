﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//need new scripts to accomodate types of attack, place reference for bullet's gameObject and behavior of the attack
//get this component and call Shoot(GameObject bullet)
public class Trajectory : MonoBehaviour {

    public Rigidbody rb;
    public Vector3 target = Vector3.zero;
    public bool debugPath;


    public float h = 25;//height
    public float gravity = -9.8f;

    // Use this for initialization
    void Start() {

        rb = GetComponent<Rigidbody>();
        gravity = Physics.gravity.y;
        
    }

    // Update is called once per frame
    void Update() {
        if (debugPath )
            DrawPath();
    }
    public void Throw(Vector3 nextTarget)
    {
        target = nextTarget;
        rb.velocity = CalculateLaunchData().initialVelocity;
    }

    //I used kinematic equation to determing the initial velocity of a trajectory by knowing its displacement of the target position on the final velocity
    //as 0 because as an object propels upwards it gain potential energy that will be a kinematic energy once it about to go down again the final velocity is always 0 for Y
    //The formula for time is calculating how much time will it take for the object to go at the highest position and time it will take to go down to its target position
    // equations (tup = sqrt(-(2height) / gravity, tdown = sqrt(2(displacement - h) / gravity), totaltime = tup + tdown, vi = displacementxz / totaltime)
    LaunchData CalculateLaunchData()
    {
        float displacementY = target.y - rb.position.y;
        Vector3 displacementXZ = new Vector3(target.x - rb.position.x, 0, target.z - rb.position.z);
        float time = Mathf.Sqrt(-2 * h / gravity) + Mathf.Sqrt(2 * (displacementY - h) / gravity);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * h);
        Vector3 velocityXZ = displacementXZ / time;

        return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
    }

    void DrawPath()
    {
        LaunchData launchData = CalculateLaunchData();
        Vector3 previousDrawPoint = rb.position;

        int resolution = 30;
        for (int i = 1; i <= resolution; i++)
        {
            float simulationTime = i / (float)resolution * launchData.timeToTarget;
            Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up * gravity * simulationTime * simulationTime / 2f;
            Vector3 drawPoint = rb.position + displacement;
            Debug.DrawLine(previousDrawPoint, drawPoint, Color.green);
            previousDrawPoint = drawPoint;
        }
    }

    struct LaunchData
    {
        public readonly Vector3 initialVelocity;
        public readonly float timeToTarget;

        public LaunchData(Vector3 initialVelocity, float timeToTarget)
        {
            this.initialVelocity = initialVelocity;
            this.timeToTarget = timeToTarget;
        }
    }
}
