﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : MonoBehaviour {

    [SerializeField] private float speed = 3f;
    [SerializeField] private float gravity = 9.8f;
    [SerializeField] private float rotationSpeed = 100f;
    [SerializeField] private float jumpSpeed = 2f;

    private CharacterController controller;
    private Animator anim;
    private Vector3 moveDirection = Vector3.zero;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetButton("Jump"))
            anim.SetBool("IsJumping", true);
        
        InputListener();
        ApplyGravity();

        controller.Move(moveDirection * Time.deltaTime);
    }

    private void InputListener()
    {
        if (!controller.isGrounded || anim.GetBool("IsJumping"))
            return;

        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

        if (moveDirection.x != 0 || moveDirection.z != 0)
        {
            anim.SetBool("IsMoving", true);

            Vector3 targetRotation = (moveDirection + transform.position) - transform.position;
            Quaternion lookRot = Quaternion.LookRotation(targetRotation);
            Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRot, Time.deltaTime * rotationSpeed).eulerAngles;
            transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }
        else
            anim.SetBool("IsMoving", false);

        moveDirection *= speed;
    }

    private void ApplyGravity()
    {
        moveDirection.y -= (gravity * Time.deltaTime);
    }

    public void Jump()
    {
        anim.SetBool("IsJumping", true);
        moveDirection /= 2;
        moveDirection.y = jumpSpeed;
    }

    public void StopJump()
    {
        anim.SetBool("IsJumping", false);
    }

    public void HorizontalSpeedZero()
    {
        moveDirection.x = 0;
        moveDirection.y = 0;
    }
}
