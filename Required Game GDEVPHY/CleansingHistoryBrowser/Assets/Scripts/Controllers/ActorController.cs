﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorController : MonoBehaviour {

    protected bool isUsed = false;

    public void Use()
    {
        isUsed = true;
        this.enabled = true;
    }

    public void SetObjectNotUsable()
    {
        isUsed = false;
        this.enabled = false;
    }
}
