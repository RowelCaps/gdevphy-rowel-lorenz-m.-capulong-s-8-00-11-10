﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalCounter : MonoBehaviour {

    [SerializeField] private GameObject canvas;

    private int currentCounter = 0;

    private void Update()
    {
        if (currentCounter >= 3)
            StartCoroutine(EndGame());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Yamete"))
            currentCounter++;

    }

    IEnumerator EndGame()
    {
        canvas.SetActive(true);

        yield return new WaitForSeconds(3f);

        SceneManager.LoadScene("asa");
    }
}
