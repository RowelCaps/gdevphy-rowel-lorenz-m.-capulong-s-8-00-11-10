﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour {

    [SerializeField] private GameObject target;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float smoothing = 5f;

    private void Update()
    {
        Vector3 targetPos = target.transform.position + offset;

        transform.position = Vector3.Lerp(transform.position, targetPos, smoothing * Time.deltaTime);
    }

}
