﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowActor : MonoBehaviour {

    [SerializeField] private float rayCastDistance = 2f;
    [SerializeField] private LayerMask mask;
    [SerializeField] private float maxThrowForce = 5f;

    private float timeCounter = 0f;

    private void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            timeCounter += Time.deltaTime;
            timeCounter = timeCounter >= maxThrowForce ? maxThrowForce : timeCounter;
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 500))
                ThrowObject(hit.point * timeCounter);
        }
        else
            timeCounter = 0;

        Ray rayDebug = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(rayDebug.origin, rayDebug.direction * 500, Color.blue);
    }

    private void ThrowObject(Vector3 target)
    {
        Ray ray = new Ray(transform.position,transform.TransformDirection(transform.forward));
        RaycastHit hit;

        if(Physics.Raycast(ray,out hit,rayCastDistance, mask))
        {
            hit.collider.gameObject.GetComponent<Trajectory>().Throw(target);
        }

        Debug.DrawRay(transform.position, transform.TransformDirection(transform.forward), Color.red);
    }
}
