﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    public Vector2 gravity = new Vector2(0f, -9.81f);

    public float mass =5f;

    private Vector2 TotalForce = Vector2.zero;
    bool isGrounded = false;
    private void Update()
    {
        ApplyForce(gravity);

        if (isGrounded)
            TotalForce.y = 0;

        transform.position += new Vector3(TotalForce.x, TotalForce.y) * Time.deltaTime;
    }

    private void ApplyForce(Vector2 force)
    {
        TotalForce += force * mass;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("fuck");
        isGrounded = true;
    }
}
