﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public GameObject fireBall;
    public Transform firePoint;
    public float angle = 3f;
    public float AngleRateChange = 2f;
    public float forceApply = 30f;
    public GameObject target;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject fire = Instantiate(fireBall) as GameObject;
            fire.GetComponent<Trajectory>().ObjectHit = target.transform;

            float y = (Mathf.Sin(angle) * Mathf.Rad2Deg);
            float x = ( Mathf.Cos(angle) * Mathf.Rad2Deg);

            x = Mathf.Abs(x);
            Vector2 scalar = new Vector2(x, y) * forceApply;

            fire.GetComponent<Move>().ApplyForce(scalar);
        }

        if(Input.GetKey(KeyCode.UpArrow))
        {
            angle += AngleRateChange;
        }
        else if(Input.GetKey(KeyCode.DownArrow))
        {
            angle -= AngleRateChange;
        }

        angle = Mathf.Clamp(angle, 0f, 90f);
    }
}
