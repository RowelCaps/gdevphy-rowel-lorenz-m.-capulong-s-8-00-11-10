﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public float gravity = -9.8f;
    public float mass = 5f;
    public float drag = 5f;

    private Vector2 TotalForce = Vector2.zero;
    private Vector2 Acceleration = Vector2.zero;

    public Vector2 GetAcceleration { get { return Acceleration; } }

    private void Update()
    {
        Decelerate();
        ApplyGravity();

        Acceleration += (TotalForce / mass) * Time.deltaTime;

        transform.position += new Vector3(Acceleration.x, Acceleration.y, 0f);
    }

    private void Decelerate()
    {
        if(TotalForce.x > 0)
            TotalForce.x -= drag * Time.deltaTime;

        if (Acceleration.x > 0)
            Acceleration.x -= drag * Time.deltaTime;

        if (Acceleration.x < 0)
            Acceleration.x = 0;
    }

    private void ApplyGravity()
    {
        TotalForce.y += gravity;
    }

    public void ApplyForce(Vector2 force)
    {
        TotalForce += force;
    }
}
