﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour {

    [SerializeField] private Transform objectHit;
    [SerializeField] private float distance = 2f;
    [SerializeField] private float applyForce = 1;

    public Transform ObjectHit { set { objectHit = value; } }

    private void Update()
    {
        if(Vector2.Distance(objectHit.position, transform.position) < distance)
        {
            objectHit.GetComponent<Move>().ApplyForce(new Vector2(applyForce, 0f));
        }
    }
}
