﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;
    public float spawnInterval = 5f;
    public float spawnRate = 3f;
    public float maxSpawn = 5f;
    public float distance = 10f;

    private float TimeCounter = 0f;

    private void Start()
    {
        StartCoroutine(spawn());
    }

    IEnumerator spawn()
    {

        for(int i = 0; i < maxSpawn; i++)
        {
            GameObject spawn = Instantiate(objectToSpawn) as GameObject;
            spawn.transform.position = SpawnPosition();

            Vector2 vel = Vector2.zero - (Vector2)spawn.transform.position;
            vel.Normalize();

            spawn.GetComponent<PhysicsAst>().ApplyForce(vel * 5f);
            yield return new WaitForSeconds(spawnRate);
        }

        yield return new WaitForSeconds(spawnInterval);
        StartCoroutine(spawn());
    }

    public Vector2 SpawnPosition()
    {
        Vector2 pos = (Random.insideUnitCircle * distance) + Vector2.one * distance;
        pos.x *= Random.Range(0, 100) <= 50 ? 1 : -1;
        pos.y *= Random.Range(0, 100) <= 50 ? 1 : -1;
        return pos;
    }
}
