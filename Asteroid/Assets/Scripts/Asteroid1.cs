﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid1 : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            GameObject ast1 = Instantiate(this.gameObject) as GameObject;
            GameObject ast2 = Instantiate(this.gameObject) as GameObject;

            ast1.transform.localScale = this.transform.localScale * .5f;
            ast2.transform.localScale = this.transform.localScale * .5f;

            Vector2 direction = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            ast1.GetComponent<PhysicsAst>().ApplyForce(direction);
            ast2.GetComponent<PhysicsAst>().ApplyForce(-direction);
            this.gameObject.SetActive(false);
        }

    }
}
