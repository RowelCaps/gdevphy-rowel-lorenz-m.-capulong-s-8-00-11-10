﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsAst : MonoBehaviour {

    public float mass = 1f;
    public float gravity = 0f;
    public Vector2 totalForce = Vector2.zero;
    public float decelerationRate = .9f;

    private void Update()
    {
        Vector3 currentForce = (totalForce * decelerationRate) + Acceleration();
        currentForce *= Time.deltaTime;
        transform.position += currentForce;
    }

    public void ApplyForce(Vector2 force)
    {
        totalForce += force;
    }

    private Vector2 Acceleration()
    {
        Vector2 accele = totalForce / mass;
        return accele;
    }
}
