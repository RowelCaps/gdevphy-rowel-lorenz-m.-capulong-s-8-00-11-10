﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public PhysicsAst physics;
    public GameObject Bullet;

    private void Start()
    {
        physics = GetComponent<PhysicsAst>();
    }

    private void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        physics.ApplyForce(input);
        FaceMouse();

        if (Input.GetMouseButtonDown(0))
            FireBullet();
    }

    private void FaceMouse()
    {
        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.up = (Vector3)mouse - transform.position ;
    }

    private void FireBullet()
    {
        GameObject bulletSpawn = Instantiate(Bullet) as GameObject;
        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mouse - (Vector2)transform.position;
        direction.Normalize();

        bulletSpawn.transform.position = transform.position;
        bulletSpawn.GetComponent<PhysicsAst>().ApplyForce(direction);
    }
}
