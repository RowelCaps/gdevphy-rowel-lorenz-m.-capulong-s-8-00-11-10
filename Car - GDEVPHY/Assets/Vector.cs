﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector {

    public float x;
    public float y;

    public Vector(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public static float convertToXCoordinates(float distance, float angle)
    {
        return Mathf.Cos(Mathf.Rad2Deg * angle) * distance;
    }

    public static float convertToYCoordinates(float distance, float angle)
    {
        return Mathf.Sin(Mathf.Rad2Deg * angle) * distance;
    }

    public static float convertToDistance(float x, float y)
    {
        return Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2));
    }

    public static float convertToAngle(float x , float y)
    {
        return Mathf.Atan2(y, x);
    }

    public void zero()
    {
        x = 0;
        y = 0;
    }
}
