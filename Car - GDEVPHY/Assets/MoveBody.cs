﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBody : MonoBehaviour {

    public float speed = 5f;
    public float mass = 5f;
    public float drag = 5f;

    Vector totalForce = new Vector(0f, 0f);
    Vector Acceleration  = new Vector(0f, 0f);

    private void Update()
    {
        Acceleration.x = Input.GetAxis("Horizontal");
        Acceleration.y = Input.GetAxis("Vertical");

        AddForce(Acceleration);

        Acceleration.x += totalForce.x / mass;
        Acceleration.y += totalForce.y / mass;

        Vector2 total = new Vector2(totalForce.x, totalForce.y);
        Vector2 currentTotalForce = Vector2.Lerp(total, Vector2.zero, drag * Time.deltaTime);
        totalForce.x = currentTotalForce.x;
        totalForce.y = currentTotalForce.y;

        print("x: " + Acceleration.x.ToString() + " Y: " + Acceleration.y.ToString());

        transform.position += new Vector3(Acceleration.x, Acceleration.y) * Time.deltaTime;
        transform.up = new Vector3(Acceleration.x, Acceleration.y) * Time.deltaTime * 2f;

    }

    private void AddForce(Vector Force)
    {
        totalForce.x += Force.x;
        totalForce.y += Force.y;
        
    }
}
