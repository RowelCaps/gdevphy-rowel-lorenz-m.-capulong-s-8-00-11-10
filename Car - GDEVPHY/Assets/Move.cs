﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    [SerializeField] float speed = 5f;
    [SerializeField] float mass = 5f;
    
    float angle = 0f;

    float accelerationX = 0f;
    float accelerationY = 0f;

    float x = 0;
    float y = 0f;

    Vector Velocity = new Vector(0,0);
	// Use this for initialization
	// Update is called once per frame
	void Update () {

        x = Input.GetAxis("Horizontal") == 0 ? x : Input.GetAxis("Horizontal");
        y = Input.GetAxis("Horizontal") == 0 ? y : Input.GetAxis("Vertical");


        angle = Vector.convertToAngle(x, y);
        Vector newVelocity = new Vector(Vector.convertToXCoordinates(x * speed, angle), Vector.convertToYCoordinates(y * speed, angle));

        applyAcceleration(newVelocity);

        Vector3 speedA = new Vector3(x, y, 0f);

        transform.up = new Vector3(x, y) * Time.deltaTime * 2f;
        transform.position += new Vector3(x + speedA.x, y + speedA.y, 0f) * Time.deltaTime;
    }

    private void applyAcceleration(Vector vel)
    {
        Vector current = new Vector(vel.x - Velocity.x, vel.y - Velocity.x);
        accelerationX += current.x * Time.deltaTime;
        accelerationY += current.y * Time.deltaTime;

        x += accelerationX;
        y += accelerationY;
    }
}
